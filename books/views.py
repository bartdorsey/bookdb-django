from django.shortcuts import render
from books.models import Book
from books.forms import BookForm

# /books/books
def show_books(request):
  # Test to see if the request is a POST vs a GET
  if request.method == 'POST':

    form = BookForm(request.POST)

    # if the form has valid data
    if form.is_valid():

      # This will make the form save the model to the database
      form.save()

  form = BookForm()
  # Get the books from the model
  books = Book.objects.all()
  context = {
    "books": books,
    "page_title": "Books",
    "form": form
  }
  return render(request, "books.html", context)


# /books
def index(request):
  return render(request, "index.html")