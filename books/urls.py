from django.urls import path
from books.views import index, show_books

# These are the urls for the books app

urlpatterns = [
  path('', index),
  path('books/', show_books)
]